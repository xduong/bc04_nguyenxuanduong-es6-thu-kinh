let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

// show list kính

let glassListContent = "";
dataGlasses.forEach(
  (item) =>
    (glassListContent +=
      /*html*/
      `<img onclick="show('${item.id}')" class='col' src=${item.src}>`)
);
document.querySelector("#vglassesList").innerHTML = glassListContent;

// show kính lên model và info

function show(id) {
  // console.log("id: ", id);
  let index = timId(id, dataGlasses);
  if (index != -1) {
    // show kính lên
    document.getElementById(
      "avatar"
    ).innerHTML = /*html*/ `<img src=${dataGlasses[index].virtualImg} />`;

    // show thông tin lên
    // b1: loại bỏ display: none
    // document.getElementById("glassesInfo").style.display("block");
    document.getElementById("glassesInfo").classList.add("d-block");
    // b2: gắn html
    document.getElementById("glassesInfo").innerHTML = /*html*/ `
    <h5>
    ${dataGlasses[index].name} - ${dataGlasses[index].brand}
    (${dataGlasses[index].color})
    </h5>
    <span class="badge badge-danger p-2">$${dataGlasses[index].price}</span>
    <span style="color: green" class="ml-1">Stocking</span>
    <p class="mb-0 mt-1">${dataGlasses[index].description}</p>
    `;

    document.querySelector("#addGlasses").setAttribute("disabled", "");
    document.querySelector("#removeGlasses").removeAttribute("disabled");
  } else {
    alert("không tìm thấy");
  }
}

function timId(id, array) {
  return array
    .map(function (e) {
      return e.id;
    })
    .indexOf(id);
}

document.querySelector("#removeGlasses").onclick = function () {
  document.querySelector("#removeGlasses").setAttribute("disabled", "");
  document.querySelector("#addGlasses").removeAttribute("disabled");

  document.getElementById("glassesInfo").classList.remove("d-block");
  document.querySelector("#avatar img").classList.add("d-none");
};

document.querySelector("#addGlasses").onclick = function () {
  document.querySelector("#addGlasses").setAttribute("disabled", "");
  document.querySelector("#removeGlasses").removeAttribute("disabled");

  document.getElementById("glassesInfo").classList.add("d-block");
  document.querySelector("#avatar img").classList.remove("d-none");
};
